﻿using MariuszScislyZaliczenie2.Models;


namespace MariuszScislyZaliczenie2.Interfaces
{
    public interface IUserService
    {
        public User AuthorizeUser(string username, string password);
        public void RegisterUser(User newUser);
    }
}
