﻿namespace MariuszScislyZaliczenie.Interfaces
{
    public interface IAppConsole
    {
        string Login();

        string Password();

        void Clear();

        string ReadLine();

        void WriteLine(object msg);

        void Write(object msg);

        int Response();

        string Data(string msg);
    }
}
